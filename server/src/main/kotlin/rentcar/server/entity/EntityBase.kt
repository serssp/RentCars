package rentcar.server.entity

import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.MappedSuperclass

@MappedSuperclass
open class EntityBase {

    var id: Int? = null
        @Id @GeneratedValue get set
}