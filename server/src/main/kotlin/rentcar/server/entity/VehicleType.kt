package rentcar.server.entity

import javax.persistence.Entity

@Entity
open class VehicleType : EntityBase() {
    open var type: String? = null
}