package rentcar.server.entity

import javax.persistence.Entity

@Entity
open class RentalPoint : EntityBase() {
    open lateinit var name: String
}