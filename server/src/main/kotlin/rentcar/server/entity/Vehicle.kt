package rentcar.server.entity

import javax.persistence.Entity
import javax.persistence.ForeignKey
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

@Entity
open class Vehicle : EntityBase() {

    open lateinit var type: VehicleType
        @ManyToOne
        @JoinColumn(name = "vehicle_type_id", foreignKey = ForeignKey(name = "VEHICLE_TYPE_FK")) get set

    open lateinit var point: RentalPoint
        @ManyToOne
        @JoinColumn(name = "rental_point_id", foreignKey = ForeignKey(name = "RENTAL_POINT_FK")) get set
}