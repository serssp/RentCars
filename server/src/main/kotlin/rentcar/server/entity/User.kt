package rentcar.server.entity

import javax.persistence.Entity

@Entity
open class User : EntityBase() {
    lateinit var firstName: String
    lateinit var lastName: String
}