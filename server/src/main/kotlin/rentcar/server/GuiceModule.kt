package rentcar.server

import com.google.inject.AbstractModule
import com.google.inject.BindingAnnotation
import com.google.inject.Provides
import rentcar.server.service.hibernate.HibernateService
import rentcar.server.service.hibernate.HibernateSettings
import rentcar.server.service.invocation.InvocationService
import rentcar.server.service.invocation.InvocationServiceImpl
import rentcar.server.service.json.JsonService
import rentcar.server.service.json.JsonServiceImpl
import rentcar.server.service.server.Server
import rentcar.server.service.server.rest.Rest
import rentcar.server.service.server.rest.RestServer
import rentcar.server.service.server.websocket.WebSocketServer

@BindingAnnotation
@Retention
@Target(AnnotationTarget.CLASS)
annotation class WebSocket


class GuiceModule : AbstractModule() {
    override fun configure() {
        bind(Server::class.java).annotatedWith(Rest::class.java).to(RestServer::class.java).asEagerSingleton()
        bind(Server::class.java).annotatedWith(WebSocket::class.java).to(WebSocketServer::class.java).asEagerSingleton()
        bind(JsonService::class.java).to(JsonServiceImpl::class.java).asEagerSingleton()
        bind(InvocationService::class.java).to(InvocationServiceImpl::class.java).asEagerSingleton()

        bind(HibernateService::class.java).toProvider(HibernateServiceProvider::class.java).asEagerSingleton()
    }

    @Provides
    fun embededDB(): HibernateSettings =
            HibernateSettings(
                    driverClass = "org.h2.Driver",
                    dialect = "org.hibernate.dialect.H2Dialect",
                    connectionUrl = "jdbc:h2:mem:",
                    user = "",
                    password = ""
            )
}