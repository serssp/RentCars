package rentcar.server.handler

import rentcar.server.entity.VehicleType
import rentcar.server.service.hibernate.HibernateService

class VehicleTypeHandler constructor(private val hibernateService: HibernateService) {

    fun addType(type: String) {
        val session = hibernateService.openSession()

        val vehicleType = VehicleType()
        vehicleType.type = type
        session.save(vehicleType)
    }

    fun list() : Array<VehicleType> {
        val session = hibernateService.openSession()
        session.entityManagerFactory.createEntityManager().
        session.createCriteria(VehicleType::class.java)
    }
}