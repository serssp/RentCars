package rentcar.server

import com.google.inject.Inject
import com.google.inject.Provider
import rentcar.server.entity.RentalPoint
import rentcar.server.entity.Vehicle
import rentcar.server.entity.VehicleType
import rentcar.server.service.hibernate.HibernateService
import rentcar.server.service.hibernate.HibernateServiceImpl
import rentcar.server.service.hibernate.HibernateSettings

class HibernateServiceProvider @Inject constructor(private val settings: HibernateSettings) : Provider<HibernateService> {
    override fun get(): HibernateService {

        return HibernateServiceImpl(settings,
                setOf(
                        VehicleType::class.java,
                        Vehicle::class.java,
                        RentalPoint::class.java
                ))
    }
}