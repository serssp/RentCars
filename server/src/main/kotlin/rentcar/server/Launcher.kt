package rentcar.server

import com.google.inject.Guice
import com.google.inject.Injector
import org.apache.logging.log4j.Level
import org.apache.logging.log4j.core.config.Configurator
import rentcar.server.service.invocation.InvocationService
import rentcar.server.service.server.rest.RestServer
import rentcar.server.service.server.websocket.WebSocketServer

fun main(args: Array<String>) {

    // TODO вынести в конфиг. Пока так, т.к. быстрее и не надо копировать в docker image
    Configurator.setRootLevel(Level.INFO)

    val injector = Guice.createInjector(GuiceModule())

    test(injector)

    // TODO переделать запуск. Избавиться от создания потока + обработка ошибок
    Thread {
        injector.getInstance(RestServer::class.java).start()
    }.start()

    injector.getInstance(WebSocketServer::class.java).start()
}

fun test(injector: Injector) {

    val invocationService = injector.getInstance(InvocationService::class.java)

    invocationService.register(TestInvoke())

}


class TestInvoke() {
    fun test(): String {
        return "Hello World!"
    }

    /*
    InvokeMessage(val objectName: String, val methodName: String, val arguments: JsonArray, val messageId: Long)
    curl -H "Content-Type: application/json" -X POST -d '{"objectName":"TestInvoke", "methodName":"test", arguments:[], messageId: 1}' localhost:8080
    */
}
