package rentcar.server.service.server.rest

import io.netty.channel.ChannelInitializer
import io.netty.channel.socket.SocketChannel
import io.netty.handler.codec.http.HttpObjectAggregator
import io.netty.handler.codec.http.HttpServerCodec
import rentcar.server.service.invocation.InvocationService
import rentcar.server.service.json.JsonService
import rentcar.server.service.server.InvokeMessageHandler
import rentcar.server.service.server.JsonEncoder

class RestServerInitializer(jsonService: JsonService,
                            invocationService: InvocationService) : ChannelInitializer<SocketChannel>() {

    private val invokeMessageHttpDecoder = InvokeMessageHttpDecoder(jsonService)
    private val invokeMessageHandler = InvokeMessageHandler(invocationService)
    private val jsonEncoder = JsonEncoder(jsonService)

    override fun initChannel(ch: SocketChannel) {
        val pipeline = ch.pipeline()
        pipeline.addLast(HttpServerCodec())
        pipeline.addLast(HttpObjectAggregator(65536))
        pipeline.addLast(ResponceHttpEncoder()) // out
        pipeline.addLast(jsonEncoder) // out

        pipeline.addLast(invokeMessageHttpDecoder) // in
        pipeline.addLast(invokeMessageHandler) // in

    }
}

