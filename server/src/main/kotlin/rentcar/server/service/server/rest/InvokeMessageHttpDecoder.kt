package rentcar.server.service.server.rest

import com.google.inject.Inject
import io.netty.channel.ChannelHandler
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.SimpleChannelInboundHandler
import io.netty.handler.codec.http.FullHttpRequest
import io.netty.handler.codec.http.HttpMethod
import rentcar.server.service.invocation.InvokeMessage
import rentcar.server.service.json.JsonService
import java.nio.charset.StandardCharsets

@ChannelHandler.Sharable
class InvokeMessageHttpDecoder @Inject constructor(
        private val jsonService: JsonService) : SimpleChannelInboundHandler<FullHttpRequest>() {

    override fun channelRead0(ctx: ChannelHandlerContext, msg: FullHttpRequest) {
        if (msg.method() == HttpMethod.POST) {
            val data = msg.content()
            val json = data.toString(StandardCharsets.UTF_8)
            val message = jsonService.gson.fromJson(json, InvokeMessage::class.java)

            ctx.fireChannelRead(message)
        }
    }
}
