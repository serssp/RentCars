package rentcar.server.service.server.websocket

import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.nio.NioServerSocketChannel
import org.apache.logging.log4j.LogManager
import rentcar.server.service.server.Server
import rentcar.server.service.server.rest.RestServerInitializer

class WebSocketServer : Server {

    override fun start() {
//        val bossGroup = NioEventLoopGroup(1)
//        val workerGroup = NioEventLoopGroup()
//        try {
//            val b = ServerBootstrap()
//            b.group(bossGroup, workerGroup)
//                    .channel(NioServerSocketChannel::class.java)
//                    .childHandler(RestServerInitializer())
//
//            val ch = b.bind(PORT).sync().channel()
//            LogManager.getLogger(this).info("WebSocketServer started")
//            ch.closeFuture().sync()
//        } finally {
//            bossGroup.shutdownGracefully()
//            workerGroup.shutdownGracefully()
//        }
    }

    companion object {
        private val PORT = 8081
    }
}
