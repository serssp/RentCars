package rentcar.server.service.server.rest

import com.google.inject.BindingAnnotation
import com.google.inject.Inject
import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.nio.NioServerSocketChannel
import org.apache.logging.log4j.LogManager
import rentcar.server.service.invocation.InvocationService
import rentcar.server.service.json.JsonService
import rentcar.server.service.server.Server

@BindingAnnotation
@Retention
@Target(AnnotationTarget.CLASS)
annotation class Rest

class RestServer @Inject constructor(private val jsonService: JsonService,
                                     private val invocationService: InvocationService) : Server {

    override fun start() {
        val bossGroup = NioEventLoopGroup(1)
        val workerGroup = NioEventLoopGroup()
        try {
            val b = ServerBootstrap()
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel::class.java)
                    .childHandler(RestServerInitializer(jsonService, invocationService))

            val ch = b.bind(PORT).sync().channel()
            LogManager.getLogger(this).info("RestServer started")
            ch.closeFuture().sync()
        } finally {
            bossGroup.shutdownGracefully()
            workerGroup.shutdownGracefully()
        }
    }

    companion object {
        private val PORT = 8080
    }
}