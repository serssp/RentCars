package rentcar.server.service.server

import com.google.inject.Inject
import io.netty.channel.ChannelHandler
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.SimpleChannelInboundHandler
import rentcar.server.service.invocation.InvocationService
import rentcar.server.service.invocation.InvokeMessage

@ChannelHandler.Sharable
class InvokeMessageHandler @Inject constructor(private val invocationService: InvocationService) : SimpleChannelInboundHandler<InvokeMessage>() {

    override fun channelRead0(ctx: ChannelHandlerContext, msg: InvokeMessage) {
        val result = invocationService.invoke(msg)
        if (result.response != null) {
            ctx.writeAndFlush(result)
        }
    }

}