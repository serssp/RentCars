package rentcar.server.service.server.rest

import io.netty.channel.ChannelHandler
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.MessageToMessageEncoder
import io.netty.handler.codec.http.DefaultFullHttpResponse
import io.netty.handler.codec.http.HttpHeaderNames
import io.netty.handler.codec.http.HttpResponseStatus
import io.netty.handler.codec.http.HttpVersion

@ChannelHandler.Sharable
class ResponceHttpEncoder : MessageToMessageEncoder<ByteArray>() {

    override fun encode(ctx: ChannelHandlerContext, msg: ByteArray, out: MutableList<Any>) {
        val responseBytes = ctx.alloc().buffer()
        responseBytes.writeBytes(msg)

        val response = DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK, responseBytes)

        response.headers().set(HttpHeaderNames.CONTENT_TYPE, "text/plain")
        response.headers().set(HttpHeaderNames.CONTENT_LENGTH, response.content().readableBytes())

        ctx.writeAndFlush(response)
    }
}