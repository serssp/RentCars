package rentcar.server.service.server

import io.netty.channel.ChannelHandler
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.MessageToMessageEncoder
import rentcar.server.service.json.JsonService

@ChannelHandler.Sharable
class JsonEncoder(private val jsonService: JsonService) : MessageToMessageEncoder<Any>() {

    override fun encode(ctx: ChannelHandlerContext, msg: Any, out: MutableList<Any>) {
        val json = jsonService.gson.toJson(msg).toByteArray()
        out.add(json)
    }
}