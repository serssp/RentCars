package rentcar.server.service.hibernate

import org.hibernate.Session
import org.hibernate.SessionFactory
import org.hibernate.boot.MetadataSources
import org.hibernate.boot.registry.StandardServiceRegistryBuilder
import javax.persistence.Persistence
import javax.persistence.EntityManagerFactory




class HibernateServiceImpl(settings: HibernateSettings, classes: Set<Class<*>>) : HibernateService {

    private val sessionFactory: SessionFactory

    init {
        val standardRegistry = StandardServiceRegistryBuilder()
                .applySetting("hibernate.hbm2ddl.auto", "update")
                .applySetting("hibernate.format_sql", "true")
                .applySetting("hibernate.connection.provider_class", "org.hibernate.connection.C3P0ConnectionProvider")
                .applySetting("hibernate.connection.driver_class", settings.driverClass)
                .applySetting("hibernate.dialect", settings.dialect)
                .applySetting("hibernate.connection.url", settings.connectionUrl)
                .build()

        val sources = MetadataSources(standardRegistry)
        classes.forEach { sources.addAnnotatedClass(it) }
        val metadata = sources.metadataBuilder.build()

        sessionFactory = metadata.sessionFactoryBuilder.build()
        val emf = Persistence.createEntityManagerFactory("CRM")
    }

    override fun openSession(): Session = sessionFactory.openSession()
}

