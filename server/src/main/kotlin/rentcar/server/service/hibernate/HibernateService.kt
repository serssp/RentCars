package rentcar.server.service.hibernate

import org.hibernate.Session

interface HibernateService {
    fun openSession(): Session
}