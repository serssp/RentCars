package rentcar.server.service.hibernate

data class HibernateSettings(
        val driverClass: String,
        val dialect: String,
        val connectionUrl: String,
        val user: String,
        val password: String
)

