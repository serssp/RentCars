package rentcar.server.service.json

import com.google.gson.Gson
import com.google.gson.GsonBuilder

interface JsonService {
    val gson: Gson
}

class JsonServiceImpl : JsonService {
    override val gson = GsonBuilder()
            .setPrettyPrinting()
            .create()

    init {
        Companion.gson = gson
    }

    companion object {
        lateinit var gson: Gson
    }
}
