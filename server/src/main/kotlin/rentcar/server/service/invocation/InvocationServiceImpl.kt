package rentcar.server.service.invocation

import com.google.gson.JsonArray
import com.google.inject.Inject
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.message.SimpleMessage
import rentcar.server.service.json.JsonService
import java.lang.reflect.Method
import java.lang.reflect.Modifier

class InvocationServiceImpl @Inject constructor(private val jsonService: JsonService) : InvocationService {

    private val handlers = HashMap<String, MethodInfo>()

    override fun register(handler: Any) {
        val className = handler.javaClass.simpleName
        handler.javaClass.methods
                .filter { Modifier.isPublic(it.modifiers) }
                .filter { it.name != "wait" }
                .forEach { putHandler(className, it.name, MethodInfo.create(handler, it)) }
    }

    override fun invoke(message: InvokeMessage): CallbackMessage {
        val methodInfo = getHandler(message.objectName, message.methodName)

        LogManager.getLogger(this).debug { SimpleMessage("invoke message: $message") }

        try {
            val invokeData = createInvokeData(methodInfo.arguments, message.arguments)
            val response = methodInfo.invoke(invokeData)

            LogManager.getLogger(this).debug { SimpleMessage("invoke result: $response") }

            return CallbackMessage(message.messageId, response)
        } catch (e: Throwable) {
            throw InvocationException(message, methodInfo)
        }
    }

    private fun createInvokeData(arguments: List<Argument>, values: JsonArray): Array<Any?> {
        return Array(arguments.size) {
            val type = arguments[it].type
            jsonService.gson.fromJson(values[it], type)
        }
    }

    private fun putHandler(className: String, methodName: String, methodInfo: MethodInfo) {
        val old = handlers.put("$className.$methodName", methodInfo)
        if (old != null) {
            throw HandlerAlreadyRegisteredException(className, methodName)
        }
    }

    private fun getHandler(className: String, methodName: String) =
            handlers["$className.$methodName"] ?: throw HandlerNotFoundException(className, methodName)


    data class MethodInfo(val handler: Any, val method: Method, val arguments: List<Argument>) {
        companion object {
            fun create(handler: Any, method: Method): MethodInfo {
                val arguments = method.parameters.map { Argument(it.type) }
                return MethodInfo(handler, method, arguments)
            }
        }

        fun invoke(arguments: Array<Any?>): Any? {
            return method.invoke(handler, *arguments)
        }
    }

    data class Argument(val type: Class<*>)

    class HandlerNotFoundException(className: String, methodName: String) : RuntimeException("$className.$methodName")
    class HandlerAlreadyRegisteredException(className: String, methodName: String) : RuntimeException("$className.$methodName")
}