package rentcar.server.service.invocation

import com.google.gson.JsonArray

/**
 * Сервис на основе [InvokeMessage] вызывает нужный хендлер.
 * Хендлером называется зарегистрированный в сервисе публичный метод
 */
interface InvocationService {
    /**
     * Зарегистрировать все публичные методы класса
     */
    fun register(handler: Any)

    /**
     * Найти и вызвать нужный метод
     * @return [CallbackMessage] результат выполнения метода + номер запроса
     */
    fun invoke(message: InvokeMessage): CallbackMessage
}

/**
 * Данные создежащие имя метода и аргументы с которые требуется его вызвать
 */
data class InvokeMessage(val objectName: String, val methodName: String, val arguments: JsonArray, val messageId: Long)

/**
 * Результат выполнения хендлера
 * [messageId] код запроса
 * [response] результат выполнения метода
 */
data class CallbackMessage(val messageId: Long, val response: Any?)

class InvocationException(message: InvokeMessage, method: InvocationServiceImpl.MethodInfo)
    : RuntimeException("Message: $message\nMethod: ${method.method}")
