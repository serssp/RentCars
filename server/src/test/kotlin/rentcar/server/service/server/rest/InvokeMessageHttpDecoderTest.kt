package rentcar.server.service.server.rest

import com.google.gson.JsonArray
import com.nhaarman.mockito_kotlin.*
import io.netty.buffer.Unpooled
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.http.FullHttpRequest
import io.netty.handler.codec.http.HttpMethod
import org.testng.Assert
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test
import rentcar.server.service.invocation.InvokeMessage
import rentcar.server.service.json.JsonServiceImpl

@Test
class InvokeMessageHttpDecoderTest {

    private lateinit var decoder: InvokeMessageHttpDecoder

    @BeforeMethod
    fun setup() {
        val jsonService = JsonServiceImpl()
        decoder = InvokeMessageHttpDecoder(jsonService)
    }

    fun shouldDecode_whenMessageIsCorrect() {
        val content = Unpooled.buffer().writeBytes("""
            {objectName: "objectA", methodName: "methodA", arguments: [], messageId=777}
        """.toByteArray())

        val request = mock<FullHttpRequest> {
            on { it.method() } doReturn HttpMethod.POST
            on { it.content() } doReturn content
        }

        val context = mock<ChannelHandlerContext>()

        decoder.channelRead(context, request)

        verify(context).fireChannelRead(check {
            Assert.assertEquals(it, InvokeMessage("objectA", "methodA", JsonArray(), 777))
        })
    }

    fun shouldNotDecode_whenMethodIsGet() {
        val request = mock<FullHttpRequest> {
            on { it.method() } doReturn HttpMethod.GET
        }

        val context = mock<ChannelHandlerContext>()

        decoder.channelRead(context, request)

        verify(context, never()).fireChannelRead(any())
    }
}
