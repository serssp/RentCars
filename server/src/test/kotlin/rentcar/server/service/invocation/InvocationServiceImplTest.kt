package rentcar.server.service.invocation

import com.google.gson.JsonArray
import com.nhaarman.mockito_kotlin.argThat
import com.nhaarman.mockito_kotlin.spy
import com.nhaarman.mockito_kotlin.verify
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test
import rentcar.server.service.json.JsonServiceImpl

@Test
class InvocationServiceImplTest {

    private lateinit var service: InvocationService
    private lateinit var handlers: ClassWithHandlers

    @BeforeMethod
    fun setup() {
        service = InvocationServiceImpl(JsonServiceImpl())

        handlers = spy<ClassWithHandlers>()
        service.register(handlers)
    }

    fun shouldInvokeHandlerA() {
        service.invoke(InvokeMessage(handlers.javaClass.simpleName, "handlerA", JsonArray(), 123))
        verify(handlers).handlerA()
    }

    fun shouldInvokeHandlerB() {
        val args = JsonArray()
        args.add(11)
        args.add("str")
        service.invoke(InvokeMessage(handlers.javaClass.simpleName, "handlerB", args, 123))

        verify(handlers).handlerB(argThat { equals(11) }, argThat { equals("str") })
    }

    @Test(expectedExceptions = arrayOf(InvocationServiceImpl.HandlerNotFoundException::class))
    fun shouldThrowException_whenHandlerNotFound() {
        service.invoke(InvokeMessage(handlers.javaClass.simpleName, "handlerAbsent", JsonArray(), 123))
    }

    @Test(expectedExceptions = arrayOf(InvocationServiceImpl.HandlerAlreadyRegisteredException::class))
    fun shouldThrowException_whenHandlerAlreadyRegistered() {
        service.register(handlers)
    }

    open class ClassWithHandlers {
        open fun handlerA() {}
        open fun handlerB(n: Int?, s: String?) {}
    }
}

